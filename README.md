SETUP

API Centro Logistico

Pre requisitos:

Maven - 3.6.2
MySqlServer 5.7 em porta 3306
MySql Workbenck
Java 8
GIT - 2.24.0.windows.2

A API Centro Logistico permite a inserção de novos pacotes via POST. Feita a inserção, ha duas possibilidade. Consultar tudo que ja foi inserido ou consultar por ID de entrega onde será feito o passo a passo para empilhar do mais pesado para o mais leve na zona de carga.

A API foi desenvolvida com SpringBoot e utiliza JPA para dados. Os testes foram cobertos com jUnit.

1 - Pré instalação

Repositorio BitBucket: https://bitbucket.org/sabino130690/

Fazer o GIT clone do Source repositorio CI&T e dar checkout na branch feature.


Na base MySQL rodar os seguintes comandos no Workbench para que a Aplicação Spring Boot esteja preparada para iniciar:


    create database db_logistico; 
 
    create user 'springuser'@'%' identified by 'P@ssw0rd';
 
    grant all on db_logistico.* to 'springuser'@'%';


2 - Iniciando a aplicação.

A. Apos executar os passos anteriores, entrar no diretorio onde foi feito o clone do projeto entrar no diretorio: spring-boot-centro-logistico e abrir o prompt de comando no caminho do mesmo conforme exemplo:

EX.: C:\springboot-workspace\ci-t\spring-boot-centro-logistico

B. Apos aberto o prompt, executar o seguinte comando: 

    mvn clean install

Este passo garante que o pacote esta integro e fara a limpeza e compilará na sequencia os fontes alem de executar os testes automaticos contidos no projeto.

C. Apos retornado BUILD SUCCESS executar o seguinte comando:

    mvn spring-boot:run

Este comando se encarregará iniciar o projeto e deixa a API funcional em http://localhost:8080

A documentação da API esta disponivel em:

https://documenter.getpostman.com/view/9446669/SW7T8BRg?version=latest#intro