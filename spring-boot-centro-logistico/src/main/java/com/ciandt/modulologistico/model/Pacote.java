package com.ciandt.modulologistico.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Pacote {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer cdPacote;
	
	private Integer deliveryid;
			
	private Long vehicle;
	
	private Integer id;
		
	public Integer getCdPacote() {
		return cdPacote;
	}

	public void setCdPacote(Integer cdPacote) {
		this.cdPacote = cdPacote;
	}

	private Double weigth;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getVehicle() {
		return vehicle;
	}

	public void setVehicle(Long vehicle) {
		this.vehicle = vehicle;
	}

	public Integer getDeliveryid() {
		return deliveryid;
	}

	public void setDeliveryid(Integer deliveryid) {
		this.deliveryid = deliveryid;
	}

	public Double getWeigth() {
		return weigth;
	}

	public void setWeigth(Double weigth) {
		this.weigth = weigth;
	}
}
