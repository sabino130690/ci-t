package com.ciandt.modulologistico.repository;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ciandt.modulologistico.model.Pacote;

@Repository
public interface PacoteRepository extends JpaRepository<Pacote, Integer> {
}