package com.ciandt.modulologistico.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ciandt.modulologistico.business.PacoteBusinessImpl;
import com.ciandt.modulologistico.domain.EntregaDTO;

@RestController
@RequestMapping({ "/package" })
public class PacoteController {

	@Autowired
	private PacoteBusinessImpl business;

	@GetMapping(path = { "/delivery/{deliveryid}/step" })
	public ResponseEntity<List> recuperarDelivery(@PathVariable Integer deliveryid) {
		return ResponseEntity.ok(business.montaEntregas(deliveryid));
	}

	@GetMapping
	public ResponseEntity<List> findAll() {
		return ResponseEntity.ok(business.buscarTudo());
	}

	@PostMapping(path = "/delivery")
	public @ResponseBody ResponseEntity insereDelivery(@RequestBody EntregaDTO entrega) {
		boolean inseriu = business.montaDadosInsert(entrega);
		if (inseriu) {
			return ResponseEntity.status(HttpStatus.CREATED).build();
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}
}