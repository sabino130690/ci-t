package com.ciandt.modulologistico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootCentroLogisticoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCentroLogisticoApplication.class, args);
	}
}
