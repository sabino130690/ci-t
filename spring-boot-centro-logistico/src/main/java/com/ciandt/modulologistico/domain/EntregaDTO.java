package com.ciandt.modulologistico.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EntregaDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -361848562459507187L;

	private Long vehicle;
	private Integer deliveryid;
	private List<PacoteDTO> pacotes;

	public List<PacoteDTO> getPacotes() {
		if (pacotes == null) {
			pacotes = new ArrayList<PacoteDTO>();
		}
		return pacotes;
	}

	public void setPacotes(List<PacoteDTO> pacotes) {
		this.pacotes = pacotes;
	}

	public Integer getDeliveryid() {
		return deliveryid;
	}

	public void setDeliveryid(Integer deliveryid) {
		this.deliveryid = deliveryid;
	}

	public Long getVehicle() {
		return vehicle;
	}

	public void setVehicle(Long vehicle) {
		this.vehicle = vehicle;
	}
}
