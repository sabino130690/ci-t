package com.ciandt.modulologistico.domain;

import java.io.Serializable;

public class RetornoDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7477491910872109377L;

	private Integer step;
	private Integer packageId;
	private String posicaoFrom;
	private String posicaoTo;

	public Integer getStep() {
		return step;
	}

	public void setStep(Integer step) {
		this.step = step;
	}

	public Integer getPackageId() {
		return packageId;
	}

	public void setPackageId(Integer packageId) {
		this.packageId = packageId;
	}

	public String getPosicaoFrom() {
		return posicaoFrom;
	}

	public void setPosicaoFrom(String posicaoFrom) {
		this.posicaoFrom = posicaoFrom;
	}

	public String getPosicaoTo() {
		return posicaoTo;
	}

	public void setPosicaoTo(String posicaoTo) {
		this.posicaoTo = posicaoTo;
	}
}