package com.ciandt.modulologistico.domain;

import java.io.Serializable;

public class PacoteDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9058175368615873953L;

	private Integer id;
	private Double weigth;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getWeigth() {
		return weigth;
	}

	public void setWeigth(Double weigth) {
		this.weigth = weigth;
	}

}
