package com.ciandt.modulologistico.business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciandt.modulologistico.domain.EntregaDTO;
import com.ciandt.modulologistico.domain.PacoteDTO;
import com.ciandt.modulologistico.domain.RetornoDTO;
import com.ciandt.modulologistico.model.Pacote;
import com.ciandt.modulologistico.repository.PacoteRepository;

@Service
public class PacoteBusinessImpl {

	@Autowired
	private PacoteRepository repository;

	public boolean montaDadosInsert(EntregaDTO entrega) {

		boolean inseriu = false;
		Pacote pacoteData = null;

		for (PacoteDTO pacote : entrega.getPacotes()) {
			pacoteData = new Pacote();
			pacoteData.setDeliveryid(entrega.getDeliveryid());
			pacoteData.setVehicle(entrega.getVehicle());
			pacoteData.setId(pacote.getId());
			pacoteData.setWeigth(pacote.getWeigth());
			repository.save(pacoteData);

			inseriu = true;
		}

		return inseriu;
	}

	public List<Pacote> buscarTudo() {
		return repository.findAll();
	}

	public List<RetornoDTO> montaEntregas(Integer deliveryid) {

		List<Pacote> pacoteComFiltro = montaListaComFiltro(repository.findAll(), deliveryid);

		return trataListaComFiltro(pacoteComFiltro);
	}

	protected List<Pacote> montaListaComFiltro(List<Pacote> pacoteSemFiltro, Integer deliveryid) {
		List<Pacote> retorno = new ArrayList<Pacote>();
		Pacote pacote = null;
		for (Pacote pack : pacoteSemFiltro) {
			pacote = new Pacote();
			if (pack.getDeliveryid().equals(deliveryid)) {
				pacote.setCdPacote(pack.getCdPacote());
				pacote.setDeliveryid(pack.getDeliveryid());
				pacote.setId(pack.getId());
				pacote.setVehicle(pack.getVehicle());
				pacote.setWeigth(pack.getWeigth());
				retorno.add(pacote);
			}
		}

		return retorno;
	}

	protected List<RetornoDTO> trataListaComFiltro(List<Pacote> pacoteComFiltro) {
		List<Integer> abastecimento = new ArrayList<Integer>();
		List<Integer> transferencia = new ArrayList<Integer>();
		List<Integer> caminhao = new ArrayList<Integer>();

		Double max = verificaMaiorPeso(pacoteComFiltro);
		Double min = verificaMenorPeso(pacoteComFiltro);

		Collections.sort(pacoteComFiltro, new Comparator<Pacote>() {
			@Override
			public int compare(Pacote c1, Pacote c2) {
				return Double.compare(c1.getWeigth(), c2.getWeigth());
			}
		});

		for (Pacote p : pacoteComFiltro) {
			abastecimento.add(p.getId());

		}

		List<RetornoDTO> rets = new ArrayList<RetornoDTO>();
		int count = 0;
		RetornoDTO retorno = null;

		boolean menor = false;
		boolean maior = false;
		int countAux = 0;

		while (count <= pacoteComFiltro.size()) {
			count++;

			for (int i = 0; i < pacoteComFiltro.size(); i++) {
				menor = false;
				maior = false;
				retorno = new RetornoDTO();
				if (pacoteComFiltro.get(i).getWeigth().equals(min)) {
					for (int c = 0; c < abastecimento.size(); c++) {
						if (abastecimento.get(c).equals(pacoteComFiltro.get(i).getId()) && menor == false) {
							countAux++;
							caminhao.add(pacoteComFiltro.get(i).getId());
							abastecimento.remove(abastecimento.indexOf(pacoteComFiltro.get(i).getId()));
							retorno.setStep(new Integer(countAux));
							retorno.setPosicaoFrom("Area de Abastecimento");
							retorno.setPosicaoTo("Area de Caminhão");
							retorno.setPackageId(pacoteComFiltro.get(i).getId());
							rets.add(retorno);
							menor = true;

						}
					}

					for (int c = 0; c < caminhao.size(); c++) {
						if (caminhao.get(c).equals(pacoteComFiltro.get(i).getId()) && menor == false) {
							countAux++;
							transferencia.add(pacoteComFiltro.get(i).getId());
							caminhao.remove(caminhao.indexOf(pacoteComFiltro.get(i).getId()));
							retorno.setStep(new Integer(countAux));
							retorno.setPosicaoFrom("Area de Caminhão");
							retorno.setPosicaoTo("Area de Transferencia");
							retorno.setPackageId(pacoteComFiltro.get(i).getId());
							rets.add(retorno);
							menor = true;
						}
					}

					for (int c = 0; c < transferencia.size(); c++) {
						if (transferencia.get(c).equals(pacoteComFiltro.get(i).getId()) && menor == false) {
							countAux++;
							abastecimento.add(pacoteComFiltro.get(i).getId());
							transferencia.remove(transferencia.indexOf(pacoteComFiltro.get(i).getId()));
							retorno.setStep(new Integer(countAux));
							retorno.setPosicaoFrom("Area de Transferencia");
							retorno.setPosicaoTo("Area de Abastecimento");
							retorno.setPackageId(pacoteComFiltro.get(i).getId());
							rets.add(retorno);
							menor = true;
						}

					}
				} else if (pacoteComFiltro.get(i).getWeigth() < max && pacoteComFiltro.get(i).getWeigth() > min) {
					for (int c = 0; c < abastecimento.size(); c++) {
						if (abastecimento.get(c).equals(pacoteComFiltro.get(i).getId()) && maior == false) {
							countAux++;
							transferencia.add(pacoteComFiltro.get(i).getId());
							abastecimento.remove(abastecimento.indexOf(pacoteComFiltro.get(i).getId()));
							retorno.setStep(new Integer(countAux));
							retorno.setPosicaoFrom("Area de Abastecimento");
							retorno.setPosicaoTo("Area de Transferencia");
							retorno.setPackageId(pacoteComFiltro.get(i).getId());
							rets.add(retorno);
							maior = true;
						}
					}
					for (int c = 0; c < transferencia.size(); c++) {
						if (transferencia.get(c).equals(pacoteComFiltro.get(i).getId()) && maior == false) {
							countAux++;
							caminhao.add(pacoteComFiltro.get(i).getId());
							transferencia.remove(transferencia.indexOf(pacoteComFiltro.get(i).getId()));
							retorno.setStep(new Integer(countAux));
							retorno.setPosicaoFrom("Area de Transferencia");
							retorno.setPosicaoTo("Area de Caminhão");
							retorno.setPackageId(pacoteComFiltro.get(i).getId());
							rets.add(retorno);
							maior = true;
						}
					}
				} else if (pacoteComFiltro.get(i).getWeigth().equals(max)) {
					for (int c = 0; c < abastecimento.size(); c++) {
						if (abastecimento.get(c).equals(pacoteComFiltro.get(i).getId())) {
							countAux++;
							caminhao.add(pacoteComFiltro.get(i).getId());
							abastecimento.remove(abastecimento.indexOf(pacoteComFiltro.get(i).getId()));
							retorno.setStep(new Integer(countAux));
							retorno.setPosicaoFrom("Area de Abastecimento");
							retorno.setPosicaoTo("Area de Caminhão");
							retorno.setPackageId(pacoteComFiltro.get(i).getId());
							rets.add(retorno);
						}
					}
				}
			}
		}

		return rets;
	}

	protected Double verificaMenorPeso(List<Pacote> pacoteComFiltro) {

		List<Pacote> pacoteVerificaMenor = pacoteComFiltro;

		Collections.sort(pacoteVerificaMenor, new Comparator<Pacote>() {
			@Override
			public int compare(Pacote c1, Pacote c2) {
				return Double.compare(c1.getWeigth(), c2.getWeigth());
			}
		});
		return pacoteVerificaMenor.get(0).getWeigth();

	}

	protected Double verificaMaiorPeso(List<Pacote> pacoteComFiltro) {
		List<Pacote> pacoteVerificaMaior = pacoteComFiltro;

		Collections.sort(pacoteVerificaMaior, new Comparator<Pacote>() {
			@Override
			public int compare(Pacote c1, Pacote c2) {
				return Double.compare(c1.getWeigth(), c2.getWeigth());
			}
		});
		return pacoteVerificaMaior.get(pacoteVerificaMaior.size() - 1).getWeigth();

	}
}