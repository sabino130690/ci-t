package com.ciandt.modulologistico.domain;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import org.junit.Test;

public class EntregaDTOTest {

	@Test
	public void EntregaDTO() {
		EntregaDTO dto = new EntregaDTO();

		dto.setDeliveryid(new Integer(1));
		dto.setVehicle(new Long(1));

		Assert.assertEquals(new Integer(1), dto.getDeliveryid());
		Assert.assertEquals(new Long(1), dto.getVehicle());

		List<PacoteDTO> pacotes = new ArrayList<PacoteDTO>();
		dto.setPacotes(pacotes);

		Assert.assertNotNull(dto.getPacotes());

		pacotes = null;
		dto.setPacotes(pacotes);

		Assert.assertNotNull(dto.getPacotes());
	}

}
