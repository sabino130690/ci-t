package com.ciandt.modulologistico.domain;

import org.junit.Assert;

import org.junit.Test;

public class PacoteDTOTest {

	@Test
	public void test() {
		PacoteDTO dto = new PacoteDTO();
		dto.setId(new Integer(1));
		dto.setWeigth(new Double(1));

		Assert.assertEquals(new Integer(1), dto.getId());
		Assert.assertEquals(new Double(1), dto.getWeigth());
	}

}
