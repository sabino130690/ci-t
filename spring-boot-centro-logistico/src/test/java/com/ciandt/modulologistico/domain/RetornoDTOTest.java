package com.ciandt.modulologistico.domain;

import org.junit.Assert;

import org.junit.Test;

public class RetornoDTOTest {

	@Test
	public void test() {
		RetornoDTO dto = new RetornoDTO();
		dto.setPackageId(new Integer(1));
		dto.setPosicaoFrom("TESTE");
		dto.setPosicaoTo("TESTE");
		dto.setStep(new Integer(1));

		Assert.assertEquals(new Integer(1), dto.getPackageId());
		Assert.assertEquals(new Integer(1), dto.getStep());
		Assert.assertEquals("TESTE", dto.getPosicaoFrom());
		Assert.assertEquals("TESTE", dto.getPosicaoTo());
	}
}