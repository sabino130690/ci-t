package com.ciandt.modulologistico.model;

import org.junit.Test;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ciandt.modulologistico.repository.PacoteRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
public class PacoteTest {

	@Autowired
	private PacoteRepository repository;

	@Test
	public void test() throws Exception {
		Pacote pacote = new Pacote();
		pacote.setCdPacote(new Integer(1));
		pacote.setDeliveryid(new Integer(1));
		pacote.setId(new Integer(1));
		pacote.setVehicle(new Long(1));
		pacote.setWeigth(new Double(1));
		repository.save(pacote);
		Assert.assertNotNull(repository.findAll());
		 
	}
}
