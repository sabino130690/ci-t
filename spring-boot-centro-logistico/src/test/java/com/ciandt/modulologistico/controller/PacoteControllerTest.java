package com.ciandt.modulologistico.controller;

import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PacoteControllerTest {

	final String BASE_URL = "http://localhost:8080/";

	@Autowired
	private PacoteController controller;

	private MockMvc mock;

	@Before
	public void setup() {
		this.mock = MockMvcBuilders.standaloneSetup(controller).build();
	}

	@Test
	public void testGet() throws Exception {
		controller = mock(PacoteController.class);

		this.mock.perform(get("/package/delivery/12345/step").accept(MediaType.parseMediaType("application/json")))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));

	}

	@Test
	public void testGetAll() throws Exception {
		controller = mock(PacoteController.class);

		this.mock.perform(get("/package").accept(MediaType.parseMediaType("application/json")))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.parseMediaType("application/json;charset=UTF-8")));

	}
}
